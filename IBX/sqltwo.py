import mysql.connector
from mysql.connector import errorcode
from extract import extract_all, cpb_links
import cpbinfo
import pickle

cxn = mysql.connector.connect(
    host='127.0.0.1',
    user='root',
    password='',
    database='fastauth'
)

cursor = cxn.cursor()

def policy_repr(info): # CpbInfo -> PolicyRepr
    d = {
        'company': info.payer,
        'policy_number': info.policy_number,
        'title': info.title,
        'policy': info.policy,
        'last_review': info.version['Version Issued'][0],
        'next_review': info.version['Version Reissued'][0],
        'url': info.url
    }
    return d

def add_policy(p): # PolicyRepr -> ()
    add = (
        "INSERT INTO ibx_policy"
        "(insurance_company, policy_number"
        ", policy_title, policy_text, last_review_date"
        ", next_review_date, policy_url) "
        "VALUES (%(company)s, %(policy_number)s, %(title)s"
        ", %(policy)s, %(last_review)s, %(next_review)s, %(url)s)"
    )
    cursor.execute(add, p)
    return

def hcpcs_repr(info): # HCPCSInfo -> HCPCSRepr
    d = {
        'hcpcs_code': info.code,
        'description': info.description,
        'category': info.category,
        'medically_necessary': int(info.medically_necessary),
        'pa_required': 0, # ignoring this field right now
        'other_information': info.other
    }
    return d

def cpt_repr(info): # CptInfo -> CptRepr
    d = {
        'cpt_code': info.code,
        'description': info.description,
        'medically_necessary': int(info.medically_necessary),
        'pa_required': 0, # ignoring this field right now
        'other_information': info.other if info.other else ''
    }
    return d

def add_cpt(c): # CptRepr -> ()
    add = (
        "INSERT INTO ibx_cpt"
        "(cpt_code, description, medically_necessary"
        ", pa_required, other_information)"
        "VALUES (%(cpt_code)d, %(description)s"
        ", %(medically_necessary)d, %(pa_required)d"
        ", %(other_information)s)"
     )
    cursor.execute(add, c)
    return

def policy_cpt_transaction(p, c): # (PolicyRepr, CptRepr) -> ()
    add_policy = (
        "INSERT INTO ibx_policy "
        "(insurance_company, policy_number"
        ", policy_title, policy_text, last_review_date"
        ", next_review_date, policy_url) "
        "VALUES (%(company)s, %(policy_number)s, %(title)s"
        ", %(policy)s, %(last_review)s, %(next_review)s, %(url)s)"
    )    
    add_cpt = (
        "INSERT INTO ibx_cpt "
        "(cpt_code, description, medically_necessary"
        ", pa_required, other_information) "
        "VALUES (%(cpt_code)d, %(description)s"
        ", %(medically_necessary)d, %(pa_required)d"
        ", %(other_information)s)"
     )

    cpt_cmd = str()
    for cpt in c:
        cpt_cmd += add_cpt % cpt
        cpt_cmd += "\nSET cptKey = LAST_INSERT_ID()\n"
        cpt_cmd += "INSERT INTO ibx_policy_cpt (policy_id, cpt_id) "
        cpt_cmd += "VALUES (policyKey, cptKey)\n"
        
    commands = (
        "START TRANSACTION\n"
        "DECLARE policyKey int\n"
        + add_policy + "\n"
        "SET policyKey = LAST_INSERT_ID()\n"
        "DECLARE cptKey int\n"
        + cpt_cmd
        # "\n" + add_cpt + "\n"
        # "SET cptKey = LAST_INSERT_ID()\n"
        # "INSERT INTO ibx_policy_cpt (policy_id, cpt_id) "
        # "VALUES (policyKey, cptKey)"
    )
    comm = commands
    print(comm)
    cursor.execute(comm, p)
    return

def add_info(info): # CpbInfo -> ()
    add_policy = (
        "INSERT INTO ibx_policy "
        "(insurance_company, policy_number"
        ", policy_title, policy_text, last_review_date"
        ", next_review_date, policy_url) "
        "VALUES (%(company)s, %(policy_number)s, %(title)s"
        ", %(policy)s, %(last_review)s, %(next_review)s, %(url)s)"
    )
    add_cpt = (
        "INSERT INTO ibx_cpt "
        "(cpt_code, description, medically_necessary"
        ", pa_required, other_information) "
        "VALUES (%(cpt_code)s, %(description)s"
        ", %(medically_necessary)s, %(pa_required)s"
        ", %(other_information)s)"
    )
    add_hcpcs = (
        "INSERT INTO ibx_hcpcs "
        "(hcpcs_code, description, medically_necessary"
        ", pa_required, other_information, category) "
        "VALUES (%(hcpcs_code)s, %(description)s"
        ", %(medically_necessary)s, %(pa_required)s"
        ", %(other_information)s, %(category)s)"
    )
    add_icd10diag = (
        "INSERT INTO ibx_icd10_diag "
        "(icd10_diag_code, description, medically_necessary"
        ", pa_required, other_information, category) "
        "VALUES (%(icd10_diag_code)s, %(description)s"
        ", %(medically_necessary)s, %(pa_required)s"
        ", %(other_information)s, %(category)s)"
    )
    add_policy_cpt = (
        "INSERT INTO ibx_policy_cpt "
        "(policy_id, cpt_id) "
        "VALUES (%s, %s)"
    )
    add_policy_hcpcs = (
        "INSERT INTO ibx_policy_hcpcs "
        "(policy_id, hcpcs_id) "
        "VALUES (%s, %s)"
    )
    add_policy_icd10diag = (
        "INSERT INTO ibx_policy_icd10_diag "
        "(policy_id, icd10_diag_id) "
        "VALUES (%s, %s)"
    )
    policy = policy_repr(info)
    cursor.execute(add_policy, policy)
    pid = cursor.lastrowid
    for cpt in info.cpt_codes: # Adding CPT Codes
        cursor.execute(add_cpt, cpt)
        print(cursor.statement)
        cid = cursor.lastrowid
        cursor.execute(add_policy_cpt, (pid, cid))
    for hcpcs in info.hcpcs_codes: # Adding HCPCS Codes
        cursor.execute(add_hcpcs, hcpcs)
        print(cursor.statement)
        hid = cursor.lastrowid
        cursor.execute(add_policy_hcpcs, (pid, hid))
    for icd10 in info.icd10_diag_codes: # Adding ICD10 Codes
        cursor.execute(add_icd10diag, icd10)
        print(cursor.statement)
        iid = cursor.lastrowid
        cursor.execute(add_policy_icd10diag, (pid, iid))
    return

def policy_cpt_hcpcs_add(info): # CpbInfo -> ()
    add_policy = (
        "INSERT INTO ibx_policy "
        "(insurance_company, policy_number"
        ", policy_title, policy_text, last_review_date"
        ", next_review_date, policy_url) "
        "VALUES (%(company)s, %(policy_number)s, %(title)s"
        ", %(policy)s, %(last_review)s, %(next_review)s, %(url)s)"
    )    
    add_cpt = (
        "INSERT INTO ibx_cpt "
        "(cpt_code, description, medically_necessary"
        ", pa_required, other_information) "
        "VALUES (%(cpt_code)s, %(description)s"
        ", %(medically_necessary)s, %(pa_required)s"
        ", %(other_information)s)"
    )
    add_hcpcs = (
        "INSERT INTO ibx_hcpcs "
        "(hcpcs_code, description, medically_necessary"
        ", pa_required, other_information, category) "
        "VALUES (%(hcpcs_code)s, %(description)s"
        ", %(medically_necessary)s, %(pa_required)s"
        ", %(other_information)s, %(category)s)"
    )
    add_policy_cpt = (
        "INSERT INTO ibx_policy_cpt "
        "(policy_id, cpt_id) "
        "VALUES (%s, %s)"
    )
    add_policy_hcpcs = (
        "INSERT INTO ibx_policy_hcpcs "
        "(policy_id, hcpcs_id) "
        "VALUES (%s, %s)"
    )
    policy = policy_repr(info)
    cursor.execute(add_policy, policy)
    pid = cursor.lastrowid
    for cpt in map(cpt_repr, info.cpt_codes):
        print(add_cpt % cpt)
        cursor.execute(add_cpt, cpt)
        print(cursor.statement)
        cid = cursor.lastrowid
        cursor.execute(add_policy_cpt, (pid, cid))
    for hcpcs in map(hcpcs_repr, info.hcpcs_codes):
        print(add_hcpcs % hcpcs)
        cursor.execute(add_hcpcs, hcpcs)
        print(cursor.statement)
        hid = cursor.lastrowid
        cursor.execute(add_policy_hcpcs, (pid, hid))
    return

def policy_cpt_add(p, c, cursor=cursor): # PolicyRepr * CptRepr -> ()
    add_policy = (
        "INSERT INTO ibx_policy "
        "(insurance_company, policy_number"
        ", policy_title, policy_text, last_review_date"
        ", next_review_date, policy_url) "
        "VALUES (%(company)s, %(policy_number)s, %(title)s"
        ", %(policy)s, %(last_review)s, %(next_review)s, %(url)s)"
    )    
    add_cpt = (
        "INSERT INTO ibx_cpt "
        "(cpt_code, description, medically_necessary"
        ", pa_required, other_information) "
        "VALUES (%(cpt_code)s, %(description)s"
        ", %(medically_necessary)s, %(pa_required)s"
        ", %(other_information)s)"
     )
    add_policy_cpt = (
        "INSERT INTO ibx_policy_cpt "
        "(policy_id, cpt_id) "
        "VALUES (%s, %s)"
    )
    cursor.execute(add_policy, p)
    pid = cursor.lastrowid
    for cpt in c:
        # print(cpt)
        print(add_cpt % cpt)
        cursor.execute(add_cpt, cpt)
        print(cursor.statement)
        cid = cursor.lastrowid
        cursor.execute(add_policy_cpt, (pid, cid))
    return


all_cpbs = extract_all('log.txt')
for title, info in all_cpbs.items():
    print("==========================================================")
    print(info.title)
    print("==========================================================")    
    # pol = policy_repr(info)
    # coz = list(map(cpt_repr, info.cpt_codes))
    try:
        add_info(info)
    except:
        print("Something went wrong with : " + title)

cxn.commit()
