import string
import re
import urllib
from bs4 import BeautifulSoup
from bs4.element import Tag, NavigableString, Comment

from utils import cache
import config

from itertools import takewhile

base_url = ('http://medpolicy.ibx.com/policies/'
            'MPI.NSF/E2DF07EC66F538A98525742700'
            '530AF4/0AC2853644329B1A85258131006'
            'E5172?OpenDocument')


resp = urllib.request.urlopen(base_url)
soup = BeautifulSoup(resp, from_encoding=resp.info().get_param('charset'))

# Section
# |> Speciality
#    |> Treatment Name
#       |> CPT Codes

section_config = {'color': '#0077C0', 'size': '4', 'face': 'Arial'}
speciality_config = {'color': '#0077C0', 'size': None}
treatment_config = {'type': 'disc'}

code_config = {'color': '#37424A'}

text_dump = list(soup.strings)
sections = soup.findAll('font', {'color': '#0077C0', 'size': '4'})
treatments = soup.findAll('ul', {'type': 'disc'})
specialities = soup.findAll('font', speciality_config)
just_codes = soup.findAll('font', code_config)
codes2 = soup.findAll('b')

def is_code(x):
    if x.name == 'b':
        if is_html(x.next_element):
            return x.next_element.attrs == {'color': '#37424A', 'face': 'Arial'}
    return False


def is_speciality(elt):
    return is_html(elt) and elt.attrs == {'color': '#0077C0', 'face': 'Arial'}

def is_section(elt):
    return elt.attrs == section_config if type(elt) is Tag else False

def is_hr(elt):
    return elt.name == 'hr' if type(elt) is Tag else False

def is_treatment(elt):
    return is_html(elt) and elt.attrs == treatment_config

def is_html(elt):
    return type(elt) is Tag
    
by_section, info = {}, {}

inf = {}

section_names = {'Services that require precertification': 'General'
                 , 'Genetic and genomic tests requiring precertification': 'Genetic'
                 , 'Specialty drugs requiring precertification': 'Drugs'}

# Transform text between sections to some intermediate representation
for s in sections:
    name = section_names[s.getText()]
    by_section[name] = []
    info[name] = {}
    inf[name] = {}
    ng = s.nextGenerator()
    special = ''
    treat = ''
    for elt in ng:
        if is_hr(elt):
            break
        if type(elt) is Tag:
            if elt.attrs == {'color': '#0077C0', 'face': 'Arial'}:
                # We are looking at a speciality
                by_section[name].append('<speciality>')
                special = elt.getText().strip()
                info[name][special] = []
                inf[name][special] = []
            if elt.attrs == {'type': 'disc'}:
                # We are looking at a treatment
                by_section[name].append('<treatment>')
                treat = elt.getText().strip()
                inf[name][special].append(elt)
        if type(elt) is NavigableString and not re.search('^\s+$', elt):
            by_section[name].append(elt)
            if special is not '' and elt != special:
                info[name][special].append(elt)

# For now just consider one section -- General
treatment_info = {}
for t in treatments:
    name = t.find('font', {'face': 'Arial'}).getText()
    treatment_info[name] = 'N/A'
    codes = t.find('b')
    if codes:
        treatment_info[name] = codes.getText()

def takeupto(fn, g):
    return takewhile(lambda x: not fn(x), g)

def get_html():
    html = {}
    for s in sections:
        name = section_names[s.getText()]
        ng = s.nextGenerator()
        html[name] = takeupto(is_hr, ng)
    return html

html_ = get_html()

