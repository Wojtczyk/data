import os
import pickle
import re
import sys
import urllib
from bs4 import BeautifulSoup
import config
from utils import cache
from cpbinfo import CpbInfo

# ---------- Extracting CPB Links ----------
# @cache('cpb_links.pickle')
def cpb_links(url=config.General['Bulletin URL']):
    # resp = urllib.request.urlopen(config.General['Bulletin URL'])
    resp = urllib.request.urlopen(url)
    bulletin = BeautifulSoup(resp, from_encoding=resp.info().get_param('charset'))
    wanted = lambda x: x.getText() and not x.getText().strip().startswith('Attachment')
    links = filter(wanted, bulletin.findAll('div', {'id': 'ViewBody'})[0].findAll('a', href=True))
    cpb = {k.getText().strip(): config.General['CPB URL Prefix'] + k.attrs['href'] for k in links}
    return cpb

def extract_all(log):
    cpb = cpb_links()
    allcpbs = {}
    for policy, url in cpb.items():
        print()
        print("Extracting information about :-")
        print(policy)
        try:
            cpbinfo = CpbInfo(url)
            allcpbs[policy] = cpbinfo
        except Exception:
            print("Couldn't extract information about :- ")
            print(policy)

    with open(log, 'w') as f:
        for policy, obj in allcpbs.items():
            f.write(str(obj) + '\n')
    return allcpbs

# if __name__ == '__main__':
    # extract_all('log.txt')
