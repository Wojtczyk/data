import string
import re
import itertools
import json
import urllib
import urllib.request
from bs4 import BeautifulSoup
from bs4.element import Tag, NavigableString, Comment
from copy import deepcopy

from utils import cache
import config

def attachment_links():
    resp = urllib.request.urlopen(config.General['Bulletin URL'])
    bulletin = BeautifulSoup(resp, from_encoding=resp.info().get_param('charset'))
    wanted = lambda x: x.getText() and x.getText().strip().startswith('Attachment')
    links = filter(wanted, bulletin.findAll('div', {'id': 'ViewBody'})[0].findAll('a', href=True))
    attachments = {k.getText().strip(): config.General['CPB URL Prefix'] + k.attrs['href'] for k in links}
    return attachments

links = attachment_links()

cpt_ = re.compile('^[0-9]{5}')
hcpcs_ = re.compile('(^[A-Z][0-9]{4})|(^[0-9]{4}[A-Z])')
icd10_ = re.compile('^[A-Z]\d{,2}\.\d+')

def code_in_attachment(attach):
    t = []
    if 'ICD' in attach:
        t.append('ICD')
    if 'CPT' in attach:
        t.append('CPT')
    if 'HCPCS' in attach:
        t.append('HCPCS')
    if not t and re.search('code', attach, re.IGNORECASE):
        t.extend(['CPT', 'ICD', 'HCPCS'])
    return t

types = {k:code_in_attachment(k) for k in links.keys()}
scrape = {k[0]: k[1] for k in links.items() if code_in_attachment(k[0])}

def get_codes(name, link):
    print("Scraping information from " + name)
    codes = {'CPT': [], 'HCPCS': [], 'ICD': []}
    resp = urllib.request.urlopen(link)
    soup = BeautifulSoup(resp)
    # lines = ''.join(list(soup.strings)).split('\n')
    lines = list(soup.strings)
    type_of_code = code_in_attachment(name)
    
    if 'CPT' in type_of_code:
        cpt_codes = list(filter(lambda x: re.search(cpt_, x), lines))
        codes['CPT'] = cpt_codes
    if 'HCPCS' in type_of_code:
        hcpcs_codes = list(filter(lambda x: re.search(hcpcs_, x), lines))
        codes['HCPCS'] = hcpcs_codes
    if 'ICD' in type_of_code:
        icd10_codes = list(filter(lambda x: re.search(icd10_, x), lines))
        codes['ICD'] = icd10_codes

    return codes

def show_codes(codes):
    for k, v in codes.items():
        print(k)
        for c in v:
            print('\t' + c)
    return

def codes_link(link):
    show_codes(get_codes('ICD HCPCS CPT', link))


def all_codes(filename='attachment_codes.json'):
    try:
        with open(filename, 'r') as f:
            return json.load(f)
    except (OSError, IOError) as e:
        codes_ = {k[0] : get_codes(k[0], k[1]) for k in scrape.items()}
        with open(filename, 'w') as f:
            json.dump(codes_, f)
        return codes_

# codes_ = {k[0] : get_codes(k[0], k[1]) for k in scrape.items()}

case1 = 'Attachment B (Codes Eligible for Reimbursement by Podiatrists) to 00.03.09d X-rays Associated with Fractures in the Office Setting'
case2 = 'http://medpolicy.ibx.com/policies/mpi.nsf/f12d23cb982d59b485257bad00552d87/85256aa800623d7a8525817700514b80!OpenDocument'
case2key = 'Attachment A (CPT, HCPCS and Revenue Codes) to 09.00.56e Radiation Therapy Services (Independence)'

attachment_codes = all_codes()
