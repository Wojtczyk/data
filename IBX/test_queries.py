import re
import mysql.connector
from mysql.connector import errorcode

cxn = mysql.connector.connect(
    host='127.0.0.1',
    user='root',
    password='',
    database='fastauth'
)

cursor = cxn.cursor()

hcpcs_code = re.compile('[A-Z][0-9]{4}')
cpt_code = re.compile('[0-9]{5}')

def cpt_by_id(cpt_id, cursor=cursor):
    query = ("SELECT cpt_code from ibx_cpt "
             "WHERE cpt_id='{}'".format(cpt_id))
    cursor.execute(query)
    return cursor.fetchall()

def hcpcs_by_id(hcpcs_id, cursor=cursor):
    query = ("SELECT hcpcs_code from ibx_hcpcs "
             "WHERE hcpcs_id='{}'".format(hcpcs_id))
    cursor.execute(query)
    return cursor.fetchall()

def icd10_diag_by_id(icd10_diag_id, cursor=cursor):
    query = ("SELECT icd10_diag_code from ibx_icd10_diag "
             "WHERE icd10_diag_id={}".format(icd10_diag_id))
    cursor.execute(query)
    return cursor.fetchall()

# Query 1
def keyword_search(keyword, cursor=cursor):
    query = ("SELECT policy_number, policy_title FROM ibx_policy "
             "WHERE (policy_title LIKE '%{}%')".format(keyword))
    cursor.execute(query)
    results = cursor.fetchall()
    print("Number       Title")
    print("------       -----")
    for (policy_number, policy_title) in results:
        print("{:9} :- {}".format(policy_number, policy_title))
    return

# Query 2
def precert_required(code, table=None, cursor=cursor):
    code_str = {'ibx_cpt': 'cpt_code', 'ibx_hcpcs': 'hcpcs_code'
                , 'ibx_icd10_diag': 'icd10_diag_code'}
    if not table:
        if re.match(cpt_code, code):
            table = 'ibx_cpt'
        elif re.match(hcpcs_code, code):
            table = 'ibx_hcpcs'
        else:
            table = 'ibx_icd10_diag'
    query = ("SELECT pa_required FROM " + table + " "
             "WHERE " + code_str[table] + "='" + str(code) + "'")
    cursor.execute(query)
    val = map(lambda x: bool(x[0]), cursor.fetchall())
    return any(val)

def cpt_by_policy_id(policy_id, cursor=cursor):
    codes = []
    query_cpt = ("SELECT cpt_id FROM ibx_policy_cpt "
                 "WHERE policy_id={}".format(policy_id))
    cursor.execute(query_cpt)
    cpt_ids = map(lambda x: x[0], cursor.fetchall())
    for i in cpt_ids:
        codes.append(cpt_by_id(i))
    return map(lambda x: x[0][0], codes)

def hcpcs_by_policy_id(policy_id, cursor=cursor):
    codes = []
    query_hcpcs = ("SELECT hcpcs_id FROM ibx_policy_hcpcs "
                   "WHERE policy_id={}".format(policy_id))
    cursor.execute(query_hcpcs)
    hcpcs_ids = map(lambda x: x[0], cursor.fetchall())
    for i in hcpcs_ids:
        codes.append(hcpcs_by_id(i))
    return map(lambda x: x[0][0], codes)

def icd10_diag_by_policy_id(policy_id, cursor=cursor):
    codes = []
    query_icd10_diag = ("SELECT icd10_diag_id from ibx_policy_icd10_diag "
                        "WHERE policy_id={}".format(policy_id))
    cursor.execute(query_icd10_diag)
    icd10_diag_ids = map(lambda x: x[0], cursor.fetchall())
    for i in icd10_diag_ids:
        codes.append(icd10_diag_by_id(i))
    return map(lambda x: x[0][0], codes)

# Query 3
def precert_by_keyword(keyword, cursor=cursor):
    query = ("SELECT policy_id, policy_title FROM ibx_policy "
             "WHERE (policy_title LIKE '%{}%')".format(keyword))
    cursor.execute(query)
    policies = cursor.fetchall()
    print('ID   Title')
    print('--   -----')
    for policy_id, policy_title in policies:
        print('\n')
        print(policy_id, policy_title)
        cpt_codes = cpt_by_policy_id(policy_id)
        for cpt in cpt_codes:
            if precert_required(cpt, 'ibx_cpt'):
                print('\tRequired for CPT code\t' + str(cpt))

        hcpcs_codes = hcpcs_by_policy_id(policy_id)
        for hcpcs in hcpcs_codes:
            if precert_required(hcpcs, 'ibx_hcpcs'):
                print('\tRequired for HCPCS code\t' + str(hcpcs))

        icd10_codes = icd10_diag_by_policy_id(policy_id)
        for icd10 in icd10_codes:
            if precert_required(icd10, 'ibx_icd10_diag'):
                print('\tRequired for ICD 10 Diagnosis code\t' + str(icd10))
    return

# Query 4
def info_by_keyword(keyword, cursor=cursor):
    query = ("SELECT policy_id, policy_title FROM ibx_policy "
             "WHERE (policy_title LIKE '%{}%')".format(keyword))
    query_cpt = ("SELECT cpt_code, description, medically_necessary"
                 ", pa_required, other_information FROM ibx_cpt "
                 "WHERE cpt_code=%s")
    query_hcpcs = ("SELECT hcpcs_code, description, medically_necessary"
                   ", pa_required, other_information, category FROM "
                   "ibx_hcpcs WHERE hcpcs_code='%s'")
    query_icd10_diag = ("SELECT icd10_diag_code, description, medically_necessary"
                        ", pa_required, other_information, category FROM "
                        "ibx_icd10_diag WHERE icd10_diag_code='%s'")
    cursor.execute(query)
    results = cursor.fetchall()
    for policy_id, policy_title in results:
        print('\n')
        print(policy_id, policy_title)
        cpt_codes = cpt_by_policy_id(policy_id)
        for cpt in cpt_codes:
            cursor.execute(query_cpt % cpt)
            for code, desc, mn, pa, other in cursor:
                print('\tCPT Code : ' + str(code))
                print('\t\tDescription         : ' + str(desc))
                print('\t\tMedically Necessary : ' + str(bool(mn)))
                print('\t\tPA Required         : ' + str(bool(pa)))
                print('\t\tOther Information   : ' + str(other))

        hcpcs_codes = hcpcs_by_policy_id(policy_id)
        for hcpcs in hcpcs_codes:
            cursor.execute(query_hcpcs % hcpcs)
            for code, desc, mn, pa, other, cat in cursor:
                print('\tHCPCS Code : ' + str(code))
                print('\t\tDescription         : ' + str(desc))
                print('\t\tCategory            : ' + str(cat))
                print('\t\tMedically Necessary : ' + str(bool(mn)))
                print('\t\tPA Required         : ' + str(bool(pa)))
                print('\t\tOther Information   : ' + str(other))

        icd10_diag_codes = icd10_diag_by_policy_id(policy_id)
        for icd10 in icd10_diag_codes:
            cursor.execute(query_icd10_diag % icd10)
            for code, desc, mn, pa, other, cat in cursor:
                print('\tHCPCS Code : ' + str(code))
                print('\t\tDescription         : ' + str(desc))
                print('\t\tCategory            : ' + str(cat))
                print('\t\tMedically Necessary : ' + str(bool(mn)))
                print('\t\tPA Required         : ' + str(bool(pa)))
                print('\t\tOther Information   : ' + str(other))
    return
