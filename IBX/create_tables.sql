CREATE TABLE IF NOT EXISTS ibx_policy (
       insurance_company       VARCHAR(25) DEFAULT 'ibx',
       policy_id	       INT NOT NULL AUTO_INCREMENT,
       policy_number	       VARCHAR(40) NOT NULL,
       policy_title	       LONGTEXT NOT NULL,
       policy_text	       LONGTEXT DEFAULT NULL,
       last_review_date	       VARCHAR(10) DEFAULT NULL,
       next_review_date	       VARCHAR(10) DEFAULT NULL,
       policy_url	       VARCHAR(255) NOT NULL,
       PRIMARY KEY	       (policy_id)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS ibx_cpt (
       cpt_id	    	       INT NOT NULL AUTO_INCREMENT,
       cpt_code		       INT(5) NOT NULL,
       description	       LONGTEXT DEFAULT NULL,
       medically_necessary     INT(1) DEFAULT 0,
       pa_required	       INT(1) DEFAULT 0,
       other_information       LONGTEXT DEFAULT NULL,
       PRIMARY KEY	       (cpt_id)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS ibx_hcpcs (
       hcpcs_id	    	       INT NOT NULL AUTO_INCREMENT,
       hcpcs_code	       VARCHAR(5) NOT NULL,
       description	       LONGTEXT DEFAULT NULL,
       medically_necessary     INT(1) DEFAULT 0,
       pa_required	       INT(1) DEFAULT 0,
       other_information       LONGTEXT DEFAULT NULL,
       PRIMARY KEY	       (hcpcs_id)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS ibx_icd10_proc (
       icd10_proc_id	       INT NOT NULL AUTO_INCREMENT,
       icd10_proc_code	       VARCHAR(10) NOT NULL,
       description	       LONGTEXT DEFAULT NULL,
       medically_necessary     INT(1) DEFAULT 0,
       pa_required	       INT(1) DEFAULT 0,
       other_information       LONGTEXT DEFAULT NULL,
       PRIMARY KEY	       (icd10_proc_id)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS ibx_icd10_diag (
       icd10_diag_id	       INT NOT NULL AUTO_INCREMENT,
       icd10_diag_code	       VARCHAR(10) NOT NULL,
       description	       LONGTEXT DEFAULT NULL,
       medically_necessary     INT(1) DEFAULT 0,
       pa_required	       INT(1) DEFAULT 0,
       other_information       LONGTEXT DEFAULT NULL,
       PRIMARY KEY	       (icd10_diag_id)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS ibx_policy_cpt (
       policy_id    	       INT NOT NULL,
       cpt_id	    	       INT NOT NULL,
       FOREIGN KEY (policy_id) REFERENCES ibx_policy (policy_id),
       FOREIGN KEY (cpt_id)    REFERENCES ibx_cpt (cpt_id)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS ibx_policy_hcpcs (
       policy_id    	       INT NOT NULL,
       hcpcs_id	    	       INT NOT NULL,
       FOREIGN KEY (policy_id) REFERENCES ibx_policy (policy_id),
       FOREIGN KEY (hcpcs_id)  REFERENCES ibx_hcpcs (hcpcs_id)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS ibx_policy_icd10_proc (
       policy_id    	            INT NOT NULL,
       icd10_proc_id	            INT NOT NULL,
       FOREIGN KEY (policy_id)      REFERENCES ibx_policy (policy_id),
       FOREIGN KEY (icd10_proc_id)  REFERENCES ibx_icd10_proc (icd10_proc_id)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS ibx_policy_icd10_diag (
       policy_id    	            INT NOT NULL,
       icd10_diag_id	            INT NOT NULL,
       FOREIGN KEY (policy_id)      REFERENCES ibx_policy (policy_id),
       FOREIGN KEY (icd10_diag_id)  REFERENCES ibx_icd10_diag (icd10_diag_id)
) ENGINE=InnoDB;
