import mysql.connector
from mysql.connector import errorcode
from extract import cpb_links
from cpbinfo import CpbInfo
import pickle

cxn = mysql.connector.connect(
    host='127.0.0.1',
    user='root',
    password='',
    database='fastauth'
)

cursor = cxn.cursor()

# The type MEDIUMTEXT can hold up to 16,777,215 characters
# The type LONGTEXT can hold up to 4,294,967,295 characters

TABLES = {}
TABLES['ibx'] = (
    "CREATE TABLE IF NOT EXISTS `ibx` ("
    "  `insurance_company` VARCHAR(25) DEFAULT 'ibx',"
    "  `policy_id` INT(5) NOT NULL AUTO_INCREMENT,"
    "  `policy_number` VARCHAR(40) NOT NULL,"
    "  `policy_title` LONGTEXT NOT NULL,"
    "  `policy_text` LONGTEXT DEFAULT NULL,"
    "  `last_review_date` VARCHAR(10) DEFAULT NULL,"
    "  `next_review_date` VARCHAR(10) DEFAULT NULL,"
    "  `policy_url` VARCHAR(255) NOT NULL,"
    "  `cpt_codes` LONGTEXT DEFAULT NULL,"
    "  `icd10_procedure_codes` LONGTEXT DEFAULT NULL,"
    "  `icd10_diagnosis_codes` LONGTEXT DEFAULT NULL,"
    "  `hcpcs_codes` LONGTEXT DEFAULT NULL,"
    "  PRIMARY KEY (`policy_id`)"
    ") ENGINE=InnoDB"
)

add_ibx = ("INSERT INTO ibx "
           "(insurance_company, policy_number "
           ", policy_title, policy_text, last_review_date "
           ", next_review_date, policy_url, cpt_codes"
           ", icd10_procedure_codes, icd10_diagnosis_codes, hcpcs_codes)"
           "VALUES (%(company)s, %(policy_number)s, %(title)s"
           ", %(policy)s, %(last_review)s, %(next_review)s, %(url)s"
           ", %(cpt)s, %(icd10p)s, %(icd10d)s, %(hcpcs)s)")


TABLES['ibxcpt'] = (
    "CREATE TABLE IF NOT EXISTS `ibxcpt` ("
    "  `policy_id` INT(5) NOT NULL AUTO_INCREMENT,"
    "  `cpt_code` LONGTEXT DEFAULT NULL,"
    "  `medically_necessary` TINYINT(1) DEFAULT 0,"
    "  `pa_required` TINYINT(1) DEFAULT 0,"
    "  `other_related` TINYINT(1) DEFAULT 0,"
    "  FOREIGN KEY (`policy_id`) REFERENCES `ibx` (`policy_id`)"
    "  ON DELETE CASCADE ON UPDATE CASCADE"
    ") ENGINE=InnoDB"
)

def create_tables(cursor):
    for name, ddl in TABLES.items():
        try:
            print("Creating table {}: ".format(name), end='')
            cursor.execute(ddl)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("already exists.")
            else:
                print(err.msg)
        else:
            print("OK")
    return

create_tables(cursor)

cpb = cpb_links()

add_ibxcpt = (
    "INSERT INTO ibxcpt "
    "(cpt_code)"
    "VALUES (%(cpt_code)s)"
)

def cpb_repr(info):
    aux = lambda x: '\n'.join(info.codes[x]) if info.codes[x] else 'N/A'
    d = {
        'company': info.payer,
        'policy_number': info.policy_number,
        'title': info.title,
        'policy': info.policy,
        'last_review': info.version['Version Issued'][0],
        'next_review': info.version['Version Reissued'][0],
        'cpt': aux('CPT'),
        'icd10p': aux('ICD 10 Procedure'),
        'icd10d': aux('ICD 10 Diagnosis'),
        'hcpcs': aux('HCPCS Level II'),
        'url': info.url
    }
    return d

# all_cpbs = {}
for policy, url in cpb.items():
    print("Extracting information about :- ")
    print(policy)
    all_cpbs[policy] = CpbInfo(url)
    print("Adding to database...")
    cursor.execute(add_ibx, cpb_repr(all_cpbs[policy]))
    # cursor.execute(add_ibxcpt, cpb_repr(all_cpbs[policy]))
    print()
