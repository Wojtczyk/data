# IBX

## about

Scraping and parsing information from [IBX](http://medpolicy.ibx.com/policies/mpi.nsf/InternetMedicalPoliciesByTitleIBC!OpenView&Start=1&Count=2000&CollapseView).

Each CPB link contains the following sections
- Policy
- Guidelines
- Description
- References
- Coding
- Cross References
- Policy History

## inventory

`config.py` : configuration options for IBX

`cpbinfo.py` : defines main `CpbInfo` object used to extract information about CPBs

`extract.py` : extracts information from all CPBs

`utils.py` : decorator to help with caching results

`log.txt` : current output file (some fields aren't displayed)

`all.pickle` : `CpbInfo` objects for each CPB

## todo

- [x] Extract text from all CPBs
- [x] Extract CPT Codes from all CPBs
- [x] Extract required fields from all CBPs
- [ ] Deal with Attachments

## dependencies

- `Python 3`
- `BeautifulSoup`
