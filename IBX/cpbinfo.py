import string
import re
import urllib
from bs4 import BeautifulSoup
from bs4.element import Tag, NavigableString, Comment

from utils import cache
from precert import precert_codes
import config

from itertools import takewhile
from collections import namedtuple

from attachments import attachment_codes

class CpbInfo(object):
    def __init__(self, url):
        self.url = url
        self.soup = self._soupify()
        self.info = self._extract_information()
        self.payer = 'IBX'

        # Helpers
        from_colon = lambda s: s[s.find(':')+1:]
        tpinfo = self._title_policy_info()

        # Properties FOR COMMERCIAL
        # self.title = from_colon(tpinfo[0].getText().strip())
        # self.policy_number = from_colon(tpinfo[1].getText().strip())

        # FOR MEDICARE
        self.title = from_colon(tpinfo[0])
        self.policy_number = from_colon(tpinfo[1])

        self.policy = self._get_info_section('Policy')
        self.description = self._get_info_section('Description')
        # self.guidelines = self._get_info_section('Guidelines')
        self.guidelines = self._get_info_section('Policy Guidelines')
        self.references = self._get_info_section('References')
        
        self.codes = self._all_codes()

        self.version = self._all_versions()

        # self.cpt_codes = cpt_infos(parse_cpt(self.soup))
        # self.cpt_codes = cpt_infos(parse(self.soup, CPTConfig()))
        # self.hcpcs_codes = hcpcs_infos(parse(self.soup, HCPCSConfig()))
        self.cpt_codes = CPTInfo(parse(self.soup, CPTConfig())).run()
        self.hcpcs_codes = HCPCSInfo(parse(self.soup, HCPCSConfig())).run()
        self.icd10_diag_codes = ICD10DiagnosisInfo(parse(self.soup, ICD10DiagnosisConfig())).run()
        
    def _extract_information(self):
        headers = self.soup.findAll('td', {'class': 'IBXHeaderPol'})
        info = {}
        for header in headers:
            name = header.getText().strip()
            info[name] = str()
            next_elements = header.nextGenerator()
            for elt in next_elements:
                for tag, transform in config.HTMLSpecialForms.items():
                    if elt.name == tag:
                        info[name] += transform
                if type(elt) is NavigableString:
                    info[name] += elt
                elif type(elt) is not Comment and elt.has_attr('class') and elt['class'] == ['IBXHeaderPol']:
                    break
        return info

    def _title_policy_info(self):
        # For Medicare
        tp = self.soup.findAll('table', {'class': 'codingtable'})
        return list(filter(None, ''.join(list(tp0.strings)).split('\n')))
        # return self.soup.findAll('table', {'class': 'codingtable'})

    def _coding_info(self):
        return list(filter(None, map(lambda x: x.strip(), self.info['Coding'].splitlines())))

    def _policy_history_info(self):
        return self._get_info_section('Policy History')

    def _get_info_section(self, section):
        return self._cleanup(self.info[section]).strip()

    def _codes(self, regex):
        cinfo = self._coding_info()
        r = re.compile(regex)
        for idx, line in enumerate(cinfo):
            if re.search(r, line):
                break
        # see if there is a better way to do this
        next_horiz = cinfo[idx:].index(config.HTMLSpecialForms['hr'].strip())
        return cinfo[idx+1:idx+next_horiz]

    def _all_codes(self):
        codes = {}
        for c, r in config.CodesRegex.items():
            codes[c] = self._codes(r)
        return codes

    def _attachment_codes(self):
        codes = {}
        for key, _codes in attachment_codes.items():
            if self.title in key:
                codes[key] = _codes

        if len(codes) == 0:
            return

        for attachment in codes:
            for cpt_code in codes[attachment]['CPT']:
                pa_req = str(cpt_code) in precert_codes
                val = {'cpt_code': cpt_code, 'description': ''
                       , 'medically_necessary': False, 'pa_required': pa_req
                       , 'other_information': attachment}
                self.cpt_codes.append(val)
            for hcpcs_code in codes[attachment]['HCPCS']:
                pa_req = str(hcpcs_code) in precert_codes
                val = {'hcpcs_code': hcpcs_code, 'description': ''
                       , 'medically_necessary': False, 'pa_required': pa_req
                       , 'category': 'Attachment', 'other_information': attachment}
                self.hcpcs_codes.append(val)

        print(codes)
        
        return codes

    def _version(self, t):
        ph = self._policy_history_info()
        v = re.findall(t, ph)
        return v if v != [] else ['N/A']

    def _all_versions(self):
        versions = {}
        for v, r in config.VersionHistoryRegex.items():
            versions[v] = self._version(r)
        return versions

    def _cleanup(self, section):
        for transform in config.HTMLSpecialForms.values():
            section = re.sub(transform, '\n', section)
        return section

    def _soupify(self):
        resp = urllib.request.urlopen(self.url)
        return BeautifulSoup(resp, from_encoding=resp.info().get_param('charset'))

    # Logging
    # Use these routines for pretty printing in the future
    def __repr__(self):
        return self.__str__()

    def __str__(self):
        s = str()
        heavy_rule = ('=' * 80)
        light_rule = ('-' * 80)
        s += heavy_rule
        s += "\nTitle: %s\n" % self.title
        s += "Policy Number: %s\n" % self.policy_number
        s += light_rule
        s += "\nCPT Codes\n" + light_rule + "\n%s" % self._str_codes('CPT')
        s += light_rule
        s += "\nICD 10 Procedure Codes:\n" + light_rule + "\n%s" % self._str_codes('ICD 10 Procedure')
        s += light_rule
        s += "\nICD 10 Diagnosis Codes:\n" + light_rule + "\n%s" % self._str_codes('ICD 10 Diagnosis')
        s += light_rule
        s += "\nHCPCS Level II Codes:\n" + light_rule + "\n%s" % self._str_codes('HCPCS Level II')
        s += light_rule
        s += "\nRevenue Codes:\n" + light_rule + "\n%s" % self._str_codes('Revenue')
        s += light_rule
        s += "\nVersion History:\n" + light_rule + "\n%s" % self._str_version()
        s += light_rule
        s += "\nPolicy Guidelines:\n" + light_rule + "\n%s\n" % "\n".join(self.guidelines.splitlines()[2:])
        s += light_rule
        s += "\nPolicy Text Blob:\n" + light_rule + "\n%s" % "\n".join(self.policy.splitlines()[2:])
        s += "\n" + heavy_rule + "\n"
        return s
    
    def _str_codes(self, t):
        codes = self.codes[t]
        s = str()
        for idx, line in enumerate(codes):
            if line.isupper():
                s += '\n' + line + '\n'
            else:
                s += line + '\n'
        return s

    def _str_version(self):
        versions = self.version
        s = str()
        for k, v in versions.items():
            s1 = k + " : " + v[0] + '\n'
            s += s1
        return s

# A functional style?

def takeupto(g, p):
    return takewhile(lambda x: not p(x), g)

def is_header(elt):
    if type(elt) is Tag and elt.has_attr('class'):
        return elt.attrs['class'] == ['IBXHeaderPol']
    return False

def get_coding(soup):
    return soup.find(class_='IBXHeaderPol', text=re.compile('Coding'))

def get_all_codes(soup):
    return soup.findAll(class_='IBXHeaderPolCoding')

def is_code_header(elt):
    if type(elt) is Tag and elt.has_attr('class'):
        return elt.attrs['class'] == ['IBXHeaderPolCoding']
    return False

class CodeConfig(object):
    def __init__(self, verbose=False):
        self.codes = {}
        self.current_heading = ''
        self.current_subheading = ''
        self.caps = re.compile('[A-Z]+')
        self.digits = re.compile('[0-9][0-9,]+')
        self.attachment = re.compile('Attachment')
        self.verbose = verbose

    def is_code_header(self, elt):
        if type(elt) is Tag and elt.has_attr('class'):
            return elt.attrs['class'] == ['IBXHeaderPolCoding']
        return False

    def start(self, elt):
        pass

    def stop(self, elt):
        if type(elt) is Tag:
            return elt.name == 'hr'
        return False

    def search(self, elt):
        return type(elt) is Tag

    def value(self):
        return self.codes

    def extract(self, elt):
        text = elt.getText().strip()
        if self._is_attachment(elt):
            self.codes = text          # this part fails on a case (search for 'Toe' in CPBs)
            # print('is_attachment: ', self.codes)
            # print('current_heading: ', self.current_heading)
            # print('current_subheading: ', self.current_subheading)
        elif self._is_heading(elt):
            if self.verbose:
                print("HEADING : " + text)
            self.current_heading = text
            self.codes[self.current_heading] = []
            self.current_subheading = ''
        elif self._is_subheading(elt) and not(self._is_heading(elt)):
            if not self.current_heading:         # added for ICD10DiagnosisConfig ! REVIEW
                self.current_subheading = ''     # added for ICD10DiagnosisConfig ! REVIEW
            elif text != self.current_heading:
                if self.verbose:
                    print("SUBHEADING : " + text)
                self.current_subheading = text
        elif self._is_codes(elt):
            if self.verbose:
                print("CODES : " + text)
            c = self.extract_codes(elt) # [CodeObjects]
            if not self.current_heading:
                self.current_heading = 'GENERAL'
            if self.current_subheading:
                t = (self.current_subheading, c)
                if self.codes[self.current_heading]:
                    self.codes[self.current_heading].extend([t])
                else:
                    self.codes[self.current_heading].append(t)
            else:
                if self.current_heading in self.codes:
                    self.codes[self.current_heading].extend(c)
                else:
                    self.codes[self.current_heading] = c
                    
    def extract_codes(self, elt):
        pass

    def _is_codes(self, elt):
        pass

    def _is_heading(self, elt):
        return elt.name == 'b' and re.search(self.caps, elt.getText())

    def _is_subheading(self, elt):
        return elt.name != 'b' and re.search(self.caps, elt.getText())

    def _is_attachment(self, elt):
        return re.search(self.attachment, elt.getText())

class CPTConfig(CodeConfig):
    def __init__(self, verbose=False):
        super().__init__(verbose)

    def start(self, elt):
        return self.is_code_header(elt) and 'CPT' in elt.getText()

    def extract_codes(self, elt):
        text = elt.getText().strip()
        return list(map(int, filter(None, text.split(','))))

    def _is_codes(self, elt):
        return re.search(self.digits, elt.getText())

class HCPCSConfig(CodeConfig):
    def __init__(self, verbose=False):
        super().__init__(verbose)
        self.sh = re.compile('[A-Z]+:$')
        self.cr = re.compile('[A-Z][0-9]{4}\s+\w+')

    def start(self, elt):
        return self.is_code_header(elt) and 'HCPCS' in elt.getText()

    def extract_codes(self, elt):
        text = elt.getText().strip()
        # return [text]
        return [(text[:5], text[5:].strip())]

    def _is_codes(self, elt):
        text = elt.getText()
        return re.search(self.cr, text) and len(text.split('\n')) == 1

    def _is_heading(self, elt):
        if type(elt) is Tag:
            return elt.name == 'b' and re.search(self.caps, elt.getText())
        return False

    def _is_subheading(self, elt):
        return re.search(self.sh, elt.getText())

class ICD10DiagnosisConfig(CodeConfig):
    def __init__(self, verbose=False):
        super().__init__(verbose)
        self.codes_regex = re.compile('[A-Z]\d{,2}.\d+\s+\w+')

    def start(self, elt):
        return self.is_code_header(elt) and 'Diagnosis' in elt.getText()

    def extract_codes(self, elt):
        text = elt.getText().strip()
        code, info = re.split('\s', text, 1)
        return [(code, info.strip())]

    def _is_codes(self, elt):
        text = elt.getText().strip()
        return re.search(self.codes_regex, text) and len(text.split('\n')) == 1

    def _is_subheading(self, elt):
        text = elt.getText().strip()
        return re.search(self.caps, text) and not self._is_codes(elt)

class ICD10ProcedureConfig(CodeConfig):
    def __init__(self):
        super().__init__()

    def start(self, elt):
        return self.is_code_header(elt) and 'Procedure' in elt.getText()

    def extract_codes(self, elt):
        pass

# parse :: BeautifulSoup * CodeConfig -> {Context : [Code]}
def parse(soup, cfg):
    start = soup.find(cfg.start)
    next_ = start.nextGenerator()
    for element in next_:
        if cfg.stop(element):
            return cfg.value()
        if cfg.search(element):
            cfg.extract(element)
    return
        

# ===== Format information to suit insertion into database =====

# an object of type CodeInfo takes the result of parse(x, CodeConfig()) and returns
# a list of code objects suitable for insertion into the database

# *NOTE, Warning*
# For all the CodeInfo objects below, checking for PA requirements is done
# by the `unit' method. There is scope for improvement in this section.

class CodeInfo(object):
    def __init__(self, codes):
        self.values = []
        self.codes = codes
        self.medically_necessary = re.compile('MEDICALLY\s+NECESSARY')

    def _medically_necessary(self, category):
        if re.search(self.medically_necessary, category):
            return 'NOT' not in category
        return False

    def _extract(self, code_object, category):
        pass

    def run(self):
        codes = self.codes
        if type(codes) is str: # See Attachment
            return self._extract(codes, 'Attachment')
        for category, codes in codes.items():
            assert type(codes) is list
            # print(category)
            for code_obj in codes:
                self._extract(code_obj, category)
        return self.values

class HCPCSInfo(CodeInfo):
    def __init__(self, codes):
        super().__init__(codes)

    def unit(self, *args):
        code = args[0]
        pa_req = code in precert_codes
        if pa_req:
            print('PA required for ' + code)
        val = {'hcpcs_code': args[0], 'description': args[1], 'category': args[2]
               , 'medically_necessary': args[3], 'pa_required': pa_req
               , 'other_information': args[5]}
        return val

    def _extract(self, code_obj, category):
        mn = self._medically_necessary(category)
        if category == 'Attachment':
            h = self.unit(-1, code_obj, category, mn, False, '')
            self.values.append(h)
        elif type(code_obj[1][0]) is tuple:
            extra = code_obj[0]
            for c in code_obj[1]:                
                h = self.unit(c[0], c[1], category, mn, False, extra)
                self.values.append(h)
        else:
            h = self.unit(code_obj[0], code_obj[1], category, mn, False, '')
            self.values.append(h)

class ICD10DiagnosisInfo(CodeInfo):
    def __init__(self, codes):
        super().__init__(codes)

    def unit(self, *args):
        code = args[0]
        pa_req = code in precert_codes
        if pa_req:
            print('PA required for ' + code)
        val = {'icd10_diag_code': args[0], 'description': args[1], 'category': args[2]
               , 'medically_necessary': args[3], 'pa_required': pa_req
               , 'other_information': args[5]}
        return val

    def _extract(self, code_obj, category):
        mn = self._medically_necessary(category)
        if category == 'Attachment':
            h = self.unit(-1, code_obj, category, mn, False, '')
            self.values.append(h)
        elif type(code_obj[1][0]) is tuple:
            extra = code_obj[0]
            for c in code_obj[1]:
                h = self.unit(c[0], c[1], category, mn, False, extra)
                self.values.append(h)
        else:
            h = self.unit(code_obj[0], code_obj[1], category, mn, False, '')
            self.values.append(h)

class CPTInfo(CodeInfo):
    def __init__(self, codes):
        super().__init__(codes)

    def unit(self, *args):
        code = args[0]
        pa_req = str(code) in precert_codes
        if pa_req:
            print('PA required for CPT Code ' + str(code))
        val = {'cpt_code': args[0], 'description': args[1]
               , 'medically_necessary': args[2], 'pa_required': pa_req
               , 'other_information': args[4]}
        return val

    def _extract(self, code_obj, category):
        mn = self._medically_necessary(category)
        if category == 'Attachment':
            self.values.append(self.unit(-1, category, mn, False, ''))
        elif type(code_obj) is tuple:
            extra = code_obj[0]
            for c in code_obj[1]:
                cinfo = self.unit(c, category, mn, False, extra)
                self.values.append(cinfo)
        else: # type is int
            cinfo = self.unit(code_obj, category, mn, False, False, '')
            self.values.append(cinfo)
