# IBX Configuration file

General = {
    'Payer': 'IBX',
    # for commercial:
    # 'Bulletin URL': ('http://medpolicy.ibx.com/policies/mpi.nsf/'
    #                  'InternetMedicalPoliciesByTitleIBC!OpenView&'
    #                  'Start=1&Count=1000&ExpandView'),

    # for medicare:
    'Bulletin URL': 'http://medpolicy.ibx.com/policies/medadvmpi.nsf/InternetMedicalPoliciesByTitleIBC!OpenView&Start=1&Count=2000&Expand=1#focus',
    
    'CPB URL Prefix': 'http://medpolicy.ibx.com'
}

HTMLSpecialForms = {
    'hr': '\n<horizontal-rule>\n'
}

CodesRegex = {
    'CPT': 'CPT\s+Procedure\s+Code\s+Number\(s\)',
    'ICD 10 Procedure': 'ICD\s*-\s*10\s+Procedure\s+Code\s+Number\(s\)',
    'ICD 10 Diagnosis': 'ICD\s*-\s*10\s+Diagnosis\s+Code\s+Number\(s\)',
    'HCPCS Level II': 'HCPCS\s+Level\s+II\s+Code\s+Number\(s\)',
    'Revenue': 'Revenue\s+Code\s+Number\(s\)'
}

VersionHistoryRegex = {
    'Version Effective': 'Version\s+Effective\s+Date\s*:\s+(\d+/\d+/\d+)',
    'Version Issued': 'Version\s+Issued\s+Date\s*:\s+(\d+/\d+/\d+)',
    'Version Reissued': 'Version\s+Reissued\s+Date\s*:\s+(\d+/\d+/\d+)'
}
