import string
import re
import itertools
import urllib
from bs4 import BeautifulSoup
from bs4.element import Tag, NavigableString, Comment

from utils import cache
import config


base_url = ('http://medpolicy.ibx.com/policies/'
            'MPI.NSF/E2DF07EC66F538A98525742700'
            '530AF4/0AC2853644329B1A85258131006'
            'E5172?OpenDocument')


resp = urllib.request.urlopen(base_url)
soup = BeautifulSoup(resp, from_encoding=resp.info().get_param('charset'))

code_config = {'color': '#37424A', 'face': 'Arial'}
code_re = re.compile('([A-Z0-9]+)')

def is_code(elt):
    if elt.name == 'b':
        ne = elt.next_element
        return type(ne) is Tag and ne.attrs == code_config
    return False

def to_code(elt):
    """
    *WARNING: Special Conditions*
    1) J9023 shows up as 'J'. This is because the HTML for J9023
       isn't the same as the HTML used for all other codes
    """
    t = elt.getText().strip()
    # *special-condition*
    if t == 'J':
        return ['J9023']
    res = re.findall(code_re, t)
    return res

codes = soup.findAll(is_code)
precert_codes = list(itertools.chain(*map(to_code, codes)))


# eviCore
evicore_url = 'http://medpolicy.ibx.com/policies/mpi.nsf/f12d23cb982d59b485257bad00552d87/2f17bdc6f056a151852582be005aa598!OpenDocument'
resp = urllib.request.urlopen(evicore_url)
soup = BeautifulSoup(resp)
s = ''.join(list(soup.strings)).split('\n')
evicore_codes = list(filter(lambda x: re.search('^[A-Z0-9]{5}$', x), s))


precert_codes = precert_codes + evicore_codes
