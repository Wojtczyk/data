import pickle

def cache(filename):
    def decorator(fn):
        def wrapped(*args, **kwargs):
            try:
                with open(filename, 'rb') as f:
                    print("Using cached result from '{}'".format(f))
                    return pickle.load(f)
            except (OSError, IOError) as e:
                res = fn(*args, **kwargs)
                with open(filename, 'wb') as f:
                    print("Caching results to '{}'".format(f))
                    pickle.dump(res, f)
        return wrapped
    return decorator
