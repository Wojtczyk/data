var express = require('express');
var mysql = require('mysql2');
var router = express.Router();

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'fastauth'
});

/* GET users listing. */
router.get('/', function(req, res, next) {
    var query = 'SELECT `policy_id`, `policy_title`, `policy_url` FROM `ibx_policy`'
    connection.connect();
    connection.query(query, (err, results) => {
	console.log("RESULTS: ", results);
	// res.render('results', {results: results.map((e) => e.policy_id + " " + e.policy_title)});
	res.render('results', {results: results});
    });
});

router.get('/cool', function(req, res, next) {
    res.send("You're so cool");
});

module.exports = router;
