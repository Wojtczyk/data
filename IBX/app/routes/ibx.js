var express = require('express');
var mysql = require('mysql2');
var router = express.Router();

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'fastauth'
});

// connection.connect();

/* Database class */
class Database {
    constructor(config) {
	this.connection = mysql.createConnection(config);
	this.connection.connect();
    }
    query(sql, args) {
	return new Promise((resolve, reject) => {
	    this.connection.query(sql, args, (err, rows) => {
		if (err)
		    return reject(err);
		resolve(rows);
	    });
	});
    }
    close() {
	return new Promise((resolve, reject) => {
	    this.connection.end(err => {
		if (err)
		    return reject(err);
		resolve();
	    });
	});
    }
}

var db = new Database({host: 'localhost', user: 'root', database: 'fastauth'});

/* GET users listing. */
router.get('/', function(req, res, next) {
    // var query = 'SELECT * FROM `ibx_policy` LIMIT 25';
    // db.query(query)
    // 	.then(results => {
    // 	    res.render('results', {  toggleUrl: '/ibx/view-all'
    // 				   , toggleText: 'View All'
    // 				   , company: 'IBX'
    // 				   , type: 'Commercial'
    // 				   , results: results  });
    // 	});
    var ibx_link = 'https://www.ibx.com/company_info/our_company/index.html'
    ibx_link = 'https://www.ibx.com/';
    res.render('company', {company: 'ibx', url: ibx_link});
});

router.get('/medical-policies', (req, res, next) => {
    var query = 'SELECT * FROM `ibx_policy` ORDER BY RAND() LIMIT 25';
    db.query(query)
	.then(results => {
	    res.render('results', {  toggleUrl: '/ibx/medical-policies/view-all'
				   , toggleText: 'View All'
				   , company: 'IBX'
				   , type: 'Commercial'
				   , results: results  });
	    
	});
});


/* Search Medical Policies */
router.post('/medical-policies/search', (req, res, next) => {
    console.log('Search request', req.body.searchterm);
    var query = "SELECT * FROM `ibx_policy` WHERE (`policy_title` LIKE '%" + req.body.searchterm + "%')";
    console.log(query);
    db.query(query)
	.then(results => {
	    var output = results;
	    if (results.length == 0)
		output = null;
	    res.render('results', {  toggleUrl: '/ibx/medical-policies/view-all'
				   , toggleText: 'View All'
				   , company: 'IBX'
				   , type: 'Commercial'
				   , results: output  });
	});
});

router.get('/medical-policies/view-all', (req, res, next) => {
    var query = 'SELECT * FROM `ibx_policy`';
    db.query(query)
	.then(results => {
	    res.render('results', {  toggleUrl: '/ibx/medical-policies'
				   , toggleText: 'View Some'
				   , company: 'IBX'
				   , type: 'Commercial'
				   , results: results  });
    });
});

router.post('/precertification/search', (req, res, next) => {
    var code = req.body.searchterm;
    console.log(code);
    var query1 = "SELECT * FROM `ibx_cpt` WHERE `pa_required`=1 AND `cpt_code`='" + code + "'";
    var query2 = "SELECT * FROM `ibx_hcpcs` WHERE `pa_required`=1 AND `hcpcs_code`='" + code + "'";
    console.log(query1);
    console.log(query2);
    var cptCodes = null;
    var hcpcsCodes = null;
    db.query(query1)
    	.then(cpts => {
    	    cptCodes = cpts;
    	    return db.query(query2);
    	})
    	.then(hcpcs => {
    	    hcpcsCodes = hcpcs;
	    if (cptCodes.length == 0)
		cptCodes = null;
	    if (hcpcsCodes.length == 0)
		hcpcsCodes = null;
    	    return res.render('precert', {company: 'ibx', cptCodes: cptCodes, hcpcsCodes: hcpcsCodes});
    	});
});

router.get('/precertification', (req, res, next) => {
    var query1 = 'SELECT * FROM `ibx_cpt` WHERE `pa_required`=1';
    var query2 = 'SELECT * FROM `ibx_hcpcs` WHERE `pa_required`=1';
    var cptCodes = null;
    var hcpcsCodes = null;
    db.query(query1)
	.then(cpts => {
	    cptCodes = cpts;
	    return db.query(query2);
	})
	.then(hcpcs => {
	    hcpcsCodes = hcpcs;
	    res.render('precert', {company: 'ibx', cptCodes: cptCodes, hcpcsCodes: hcpcsCodes});
	});
});


router.get('/medical-policies/:policyId', (req, res, next) => {
    var query = 'SELECT * FROM `ibx_policy` WHERE `policy_id`=' + req.params.policyId;
    // cpts = getCPTCodes(req.params.policyId, connection);
    // console.log("CPT IDS: ", cpts);
    db.query(query)
	.then(results => {
	    let policy_id = results[0].policy_id;
	    getCodes(policy_id, 'cpt', (cpt_codes) => {
		getCodes(policy_id, 'hcpcs', (hcpcs_codes) => {
		    getCodes(policy_id, 'icd10_diag', (icd10_diag_codes) => {
		    	res.render('policy', {company: 'IBX'
					      , policy: results[0]
					      , cptCodes: cpt_codes
					      , hcpcsCodes: hcpcs_codes
					      , icd10Codes: icd10_diag_codes});			
		    });
		});
	    });
	});
});

function getPrecertCodes(callback) {
    var query1 = 'SELECT * FROM `ibx_cpt` WHERE `pa_required`=1';
    var query2 = 'SELECT * FROM `ibx_hcpcs` WHERE `pa_required`=1';
    console.log(query1);
    console.log(query2);
    db.query(query1)
	.then(cpt => {
	    return db.query(query2);
	})
	.then(hcpcs => {
	    callback(cpt, hcpcs)
	});
}


function getPolicy(code_id, code_type, callback) {
    var query1 = 'SELECT `policy_id` FROM `ibx_policy_' + code_type + '` '
	+ 'WHERE `' + code_type + '_id`=' + code_id;
    db.query(query1)
	.then(policy_ids => {
	    var query2 = 'SELECT * FROM `ibx_policy` WHERE `policy_id` in (';
	    if (policy_ids.length == 0)
		return null;
	    for (let i = 0; i < policy_ids.length; i++) {
		query2 += policy_ids[i].policy_id;
		if (i != policy_ids.length-1)
		    query2 += ',';
	    }
	    query2 += ')';
	    // console.log(query2);
	    return db.query(query2);
	})
	.then(policies => {
	    callback(policies);
	});
}


function getCodes(policy_id, code_type, callback) {
    var codes = [];
    var query1 = 'SELECT `' + code_type + '_id` FROM `ibx_policy_' + code_type + '`'
	+ ' WHERE `policy_id`=' + policy_id;
    // console.log("query: ", query1);
    db.query(query1)
	.then(codes => {
	    var query2 = 'SELECT * FROM `ibx_' + code_type + '` WHERE `' + code_type + '_id` in (';
	    if (codes.length == 0)
		return null;
	    for (let i = 0; i < codes.length; i++) {
		query2 += codes[i][code_type + '_id'];
		if (i != codes.length-1)
		    query2 += ',';
	    }
	    query2 += ')';
	    console.log(query2);
	    return db.query(query2);
	})
	.then(code_objs => {
	    callback(code_objs);
	});
}

function getCPTCodes(policy_id, callback) {
    var codes = [];
    var cptIds = null;
    var query1 = 'SELECT `cpt_id` FROM `ibx_policy_cpt` WHERE `policy_id`=' + policy_id;
    console.log(query1);
    db.query(query1)
	.then(cpt_ids => {
	    cptIds = cpt_ids;
	    var query2 = 'SELECT * FROM `ibx_cpt` WHERE `cpt_id` in (';
	    if (cpt_ids.length == 0)
		return null;
	    for (let i = 0; i < cpt_ids.length; i++) {
		if (i == cpt_ids.length-1)
		    query2 += cpt_ids[i].cpt_id
		else
		    query2 += cpt_ids[i].cpt_id + ',';
	    }
	    query2 += ')';
	    console.log(query2);
	    return db.query(query2);
	})
	.then(cpt_objs => {
	    codes = cpt_objs;
	    callback(codes);
	});
}


module.exports = router;
