HCPCSInfo = namedtuple("HCPCSInfo", "code description category medically_necessary pa_required other")

def hcpcs_infos(hcpcs_codes): # Dict -> [HCPCSInfo]
    hcpcs = []
    def __extract(code_obj, cat, mness):
        if type(code_obj[1][0]) is tuple:
            extra = code_obj[0]
            for c in code_obj[1]:
                # print(c)
                h = HCPCSInfo(c[0], c[1], cat, mness, False, extra)
                hcpcs.append(h)
        elif type(code_obj) is tuple:
            h = HCPCSInfo(code_obj[0], code_obj[1], cat, mness, False, '')
            hcpcs.append(h)

    mn = re.compile('MEDICALLY\s+NECESSARY')
    if type(hcpcs_codes) is str: # See Attachment
        return [HCPCSInfo(-1, hcpcs_codes, '', False, False, '')]
    for category, codes in hcpcs_codes.items():
        assert type(codes) is list
        if re.search(mn, category) and 'NOT' not in category:
            for code_obj in codes:
                __extract(code_obj, category, True)
        else:
            for code_obj in codes:
                __extract(code_obj, category, False)
    return hcpcs

CptInfo = namedtuple("CptInfo", "code description medically_necessary pa_required other")

def cpt_infos(cpt_codes): # Dict -> [CptInfo]
    cpts = []
    mn = re.compile('MEDICALLY\s+NECESSARY')
    if type(cpt_codes) is str: # See Attachment ...
        return [CptInfo(-1, cpt_codes, False, False, '')]
    for category, codes in cpt_codes.items():
        assert type(codes) is list
        if category == 'GENERAL': # Only information is the CPT code
            for code_obj in codes:
                if type(code_obj) is tuple:
                    extra = code_obj[0]
                    for c in code_obj[1]:
                        cinfo = CptInfo(c, 'GENERAL', False, False, extra)
                        cpts.append(cinfo)
                elif type(code_obj) is int:
                    cinfo = CptInfo(code_obj, 'GENERAL', False, False, '')
                    cpts.append(cinfo)
        elif re.search(mn, category) and 'NOT' not in category:
            for code_obj in codes:
                if type(code_obj) is tuple:
                    extra = code_obj[0]
                    for c in code_obj[1]:
                        cinfo = CptInfo(c, category, True, False, extra)
                        cpts.append(cinfo)
                elif type(code_obj) is int:
                    cinfo = CptInfo(code_obj, category, True, False, '')
                    cpts.append(cinfo)
        else:
            for code_obj in codes:
                if type(code_obj) is tuple:
                    extra = code_obj[0]
                    for c in code_obj[1]:
                        cinfo = CptInfo(c, category, False, False, extra)
                        cpts.append(cinfo)
                elif type(code_obj) is int:
                    cinfo = CptInfo(code_obj, category, False, False, '')
                    cpts.append(cinfo)
    return cpts

def showCPT(codes):
    for k, v in codes.items():
        print(k, '-->')
        for it in v:
            if type(it) is tuple:
                print('\t', it[0])
                for it1 in it[1]:
                    print('\t\t', it1)
            else:
                print('\t', it)
                    

def showHCPCS(codes):
    for k, v in codes.items():
        print(k, '-->')
        for it in v:
            if type(it) is tuple:
                print('\t', it[0])
                for it1 in it[1]:
                    print('\t\t', it1[:5], '-->', it1[5:].strip())
            else:
                print('\t', it[:5], '-->', it[5:].strip())

# Use this to parse other codes as well
def parse_cpt(soup):
    cpt = soup.find(is_code_header) # The first is CPT codes
    ng = cpt.nextGenerator()
    caps = re.compile('[A-Z]+')
    digits = re.compile('[0-9][0-9,]+')
    attach = re.compile('Attachment')
    info = {}
    heading = ''
    sub_heading = ''
    for elt in ng:
        if elt.name == 'hr':
            break
        if type(elt) is Tag:
            txt = elt.getText().strip()
            print(txt)
            if re.search(attach, txt):
                return txt # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            elif elt.name == 'b' and re.search(caps, txt): # header
                heading = txt
                info[heading] = []
                sub_heading = ''
            elif re.search(caps, txt): # sub header
                if txt != heading:
                    sub_heading = txt
                    # info[heading].append(sub_heading)
            elif re.search(digits, txt):
                # extract the digits
                print(txt)
                codes = list(map(int, filter(None, txt.split(','))))
                print(heading)
                print(sub_heading)
                if not heading:
                    heading = 'GENERAL'
                if sub_heading:
                    t = (sub_heading, codes)
                    if info[heading]:
                        info[heading].extend([t])
                    else:
                        info[heading].append(t)
                else:
                    if heading in info.keys():
                        info[heading].extend(codes)
                    else:
                        info[heading] = codes
    return info
